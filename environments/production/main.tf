terraform {
  backend "consul" {
    path = "terraform/media-streaming"
  }

  required_providers {
    nomad = {
      source = "hashicorp/nomad"
      version = "1.4.15"
    }
    vault = {
      source = "hashicorp/vault"
      version = "3.2.1"
    }
  }
}

provider "nomad" {}
provider "vault" {
  skip_child_token = true // already a limited use token
}
