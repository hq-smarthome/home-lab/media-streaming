locals {
  plugin_id = "axion-proxima"
  volume_id = "plex-data"
}

data "nomad_plugin" "axion_proxima" {
  plugin_id        = local.plugin_id
  wait_for_healthy = false
}

data "vault_generic_secret" "axion_proxima_auth" {
  path = "cluster-storage/proxima"
}

resource "nomad_external_volume" "plex_data" {
  depends_on  = [data.nomad_plugin.axion_proxima]

  type        = "csi"
  namespace   = "default"
  plugin_id   = local.plugin_id

  name        = local.volume_id
  volume_id   = local.volume_id

  capacity_min = "1GiB"
  capacity_max = "20GiB"

  capability {
    access_mode     = "single-node-reader-only"
    attachment_mode = "file-system"
  }

  capability {
    access_mode     = "single-node-writer"
    attachment_mode = "file-system"
  }

  mount_options {
    fs_type = "cifs"
    mount_flags = ["vers=3", format("uid=%s", var.user_id), format("gid=%s", var.group_id), "nolock", format("username=%s", data.vault_generic_secret.axion_proxima_auth.data["AXION_USER"]), format("password=%s", data.vault_generic_secret.axion_proxima_auth.data["AXION_PASS"])]
  }
}
