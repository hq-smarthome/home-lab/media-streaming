job "media-streaming" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]
  namespace = "default"

  // group "tautulli" {
  //   count = 1

  //   volume "tautulli" {
  //     type = "host"
  //     source = "tautulli"
  //     read_only = false
  //   }

  //   network {
  //     mode = "bridge"
  //   }

  //   service {
  //     name = "tautulli"
  //     port = "8181"

  //     connect {
  //       sidecar_service {
  //         proxy {
  //           upstreams {
  //             destination_name = "plex"
  //             local_bind_port  = 32400
  //           }
  //         }
  //       }
  //     }

  //     tags = [
  //       "traefik.enable=true",
  //       "traefik.consulcatalog.connect=true",
  //       "traefik.http.routers.tautulli.entrypoints=https",
  //       "traefik.http.routers.tautulli.tls=true",
  //       "traefik.http.routers.tautulli.tls.certresolver=lets-encrypt",
  //       "traefik.http.routers.tautulli.tls.domains[0].main=*.hq.carboncollins.se"
  //     ]
  //   }

  //   task "tautulli" {
  //     driver = "docker"

  //     config {
  //       image = "[[ .tautulliImage ]]"
  //     }

  //     volume_mount {
  //       volume = "tautulli"
  //       destination = "/config"
  //       read_only = false
  //     }

  //     template {
  //       data = <<EOH
  //         TZ='Europe/Stockholm'
  //         PUID=1005
  //         PGID=1005
  //       EOH

  //       destination = "secrets/config.env"
  //       env = true
  //     }
  //   }
  // }

  group "plex" {
    count = 1

    volume "axion-media" {
      type = "host"
      source = "axion-media"
      read_only = false
    }

    volume "plex-data" {
      type = "csi"
      source = "plex-data"
      read_only = false

      attachment_mode = "file-system"
      access_mode = "single-node-writer"
    }

    network {
      mode = "bridge"

      port "plex" {
        to = 32400
      }
    }

    service {
      name = "plex"
      port = "32400"
      task = "plex"

      check {
        expose = true
        name = "Application Health Status"
        type = "http"
        path = "/identity"
        interval = "10s"
        timeout = "3s"
      }

      connect {
        sidecar_service {}
      }

      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=true",
        "traefik.http.routers.plex.entrypoints=https",
        "traefik.http.routers.plex.tls=true",
        "traefik.http.routers.plex.tls.certresolver=lets-encrypt",
        "traefik.http.routers.plex.tls.domains[0].main=*.hq.carboncollins.se"
      ]
    }

    task "plex" {
      driver = "docker"

      config {
        image = "[[ .plexImage ]]"

        runtime = "nvidia"
      }

      volume_mount {
        volume = "axion-media"
        destination = "/media/axion/media"
      }

      volume_mount {
        volume = "plex-data"
        destination = "/config"
      }

      resources {
        cores = 1
        memory = 4096

        device "nvidia/gpu" {
          count = 1
        }
      }

      template {
        data = <<EOH
          TZ='Europe/Stockholm'
          PUID=[[ .defaultUserId ]]
          PGID=[[ .defaultGroupId ]]
          VERSION=docker
        EOH

        destination = "secrets/config.env"
        env = true
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
