# Media Streaming

[[_TOC_]]

## Description

This repository contains configurations for [Plex](https://plex.tv/) which is used to stream media
within hQ.

## Job

This repo submits a job to the nomad cluster which defines all of the nessasary service requirements to run.

### Plex

Streams media from the `media` share in Axion

#### Docker

This job uses the [linuxserver/plex](https://fleet.linuxserver.io/image?name=linuxserver/plex)
docker image provided by [LinuxServer.io](https://www.linuxserver.io/).

### Tautulli

Gathers metrics from plex

#### Docker

This job uses the [linuxserver/tautulli](https://fleet.linuxserver.io/image?name=linuxserver/tautulli)
docker image provided by [LinuxServer.io](https://www.linuxserver.io/).

## CI/CD

Configurations are automatically deployed on changes to the `main` branch in this
repository. Pipeline configuration is imported from the
[cicd-template repository](https://gitlab.com/hq-smarthome/home-lab/cicd-template) which defines a
common ci pipeline.
